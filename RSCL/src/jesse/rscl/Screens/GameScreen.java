package jesse.rscl.Screens;

import jesse.rscl.RSCL;
import jesse.rscl.Items.ItemManager;
import jesse.rscl.NPC.NPC;
import jesse.rscl.NPC.NPCManager;
import jesse.rscl.Nodes.NodeManager;
import jesse.rscl.Player.Player;
import jesse.rscl.Quests.QuestManager;
import jesse.rscl.UI.TabBar;
import jesse.rscl.UI.UIManager;
import jesse.rscl.Util.Log;
import jesse.rscl.map.MapManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class GameScreen implements Screen {
	float w, h;
	public static OrthographicCamera camera;
	public SpriteBatch batch;
	public static Player player;
	public static MapManager mapManager;
	public static UIManager uiManager;
	public static ItemManager iManager;
	public static Log log;
	public TabBar tabBar;
	public static NodeManager nodeManager;
	public static NPCManager npcManager;
	public static QuestManager questManager;
	

	public GameScreen(RSCL game) {
		w = Gdx.graphics.getWidth();
		h = Gdx.graphics.getHeight();

		camera = new OrthographicCamera(Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight());

		camera.setToOrtho(false, w, h);
		camera.translate(0,-4);
		camera.update();

		// Let's not create another SpriteBatch but rather fetch the existing
		// one.
		batch = game.getBatch();

		// Create a new map
		mapManager = new MapManager();
		
		
		//Create items
		iManager = new ItemManager();
		
		// Create our player
		player = new Player();
		
		//Temp log
		log = new Log();
		
		tabBar = new TabBar();
		
		//Nodes
		nodeManager = new NodeManager();
		
		//Add NPCs
		npcManager = new NPCManager();
		
		//Add quests
		questManager = new QuestManager();
		
		//Create new UIManager
		uiManager = new UIManager();
		
		player.load();
		
		// Add input processor
		Gdx.input.setInputProcessor(new InputMultiplexer(player.playerInput,uiManager.stage));
	}

	@Override
	public void render(float delta) {
		// Clear the screen.
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		player.update(this);

		camera.update();
		mapManager.update(camera);
		
		uiManager.update();
		nodeManager.update();

		// Draw to screen
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
			batch.draw(player.playerSprite, player.playerPosition.x*32,
				player.playerPosition.y*32);
			//render NPCs
			for(NPC npc : npcManager.NPCs){
				batch.draw(npc.npcSprite,npc.npcPosition.getX()*32,npc.npcPosition.getY()*32);
			}
		
		batch.end();
		
		//UI Rendering
		uiManager.stage.draw();
		
		//TabBar
		tabBar.render();
		
		
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
