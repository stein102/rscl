package jesse.rscl;

import jesse.rscl.Screens.GameScreen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class RSCL extends Game {
	private SpriteBatch batch;
	FPSLogger fps;
	
	@Override
	public void create() {		
		fps = new FPSLogger();
		batch = new SpriteBatch();
		
		setScreen(new GameScreen(this));
	
	}
	
	public SpriteBatch getBatch() { return batch; }

	@Override
	public void dispose() {
		batch.dispose();
		super.dispose();
	}

	@Override
	public void render() {		
		super.render();
		fps.log();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
}
