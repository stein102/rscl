package jesse.rscl.Util;

import java.io.IOException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;


public class ResolutionHandler {

	private final int defaultWidth = 1600;
	private final int defaultHeight = 900;
	private final double defaultRatio = defaultWidth/defaultHeight;
	
	private int currentWidth;
	private int currentHeight;
	
	private double Scale = currentWidth/defaultWidth;
	
	public ResolutionHandler() {
		XmlReader reader = new XmlReader();
		Element root = null;
		try {
			root = reader.parse(Gdx.files.internal("data/config.xml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Array<Element> elements = root.getChildrenByName("config");
		
		for(Element e : elements){
			
		}
		
	}

	public int getCurrentWidth() {
		return currentWidth;
	}

	public void setCurrentWidth(int currentWidth) {
		this.currentWidth = currentWidth;
	}

	public int getCurrentHeight() {
		return currentHeight;
	}

	public void setCurrentHeight(int currentHeight) {
		this.currentHeight = currentHeight;
	}

	public double getScale() {
		return Scale;
	}

	public void setScale(double Scale) {
		this.Scale = Scale;
	}

	public int getDefaultWidth() {
		return defaultWidth;
	}

	public int getDefaultHeight() {
		return defaultHeight;
	}

	public double getDefaultRatio() {
		return defaultRatio;
	}
	
	
	
}
