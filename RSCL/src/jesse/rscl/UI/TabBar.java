package jesse.rscl.UI;

import jesse.rscl.Screens.GameScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class TabBar {

	Texture inventoryTab,equipTab,skillTab,questTab;
	OrthographicCamera camera;
	SpriteBatch batch;
	int x = 1273, y = 526;
	int tabOffset = 50;
	int state = 0;
	
	//Define states
	final int INVENTORY = 0;
	final int EQUIP = 1;
	final int SKILL = 2;
	final int QUEST = 3;
	
	public TabBar() {
		inventoryTab = new Texture(Gdx.files.internal("data/inventoryTab.png"));
		equipTab = new Texture(Gdx.files.internal("data/equipTab.png"));
		skillTab = new Texture(Gdx.files.internal("data/skillTab.png"));
		questTab = new Texture(Gdx.files.internal("data/questTab.png"));
		
		camera = new OrthographicCamera();
		batch = new SpriteBatch();
		
		camera = new OrthographicCamera(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		
		camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		camera.translate(0,-4);
		camera.update();
	}
	
	public void render(){
		camera.update();
		
		batch.begin();
		//Render icons

		batch.draw(inventoryTab,x,y);
		batch.draw(equipTab,x+(tabOffset*1),y);
		batch.draw(skillTab,x+(tabOffset*2),y);
		batch.draw(questTab,x+(tabOffset*3),y);
		
		batch.end();
		
		batch.begin();
		
		switch(state){
		case INVENTORY:
			GameScreen.player.playerInventory.render();
			break;
		case EQUIP:
			GameScreen.player.playerEquipment.render();
			break;
		case SKILL:
			GameScreen.player.playerSkills.render();
			break;
		case QUEST:
			GameScreen.uiManager.questUI.render();
			break;
		}
		
		batch.end();
	}
	
	public void clicked(int screenX, int screenY, int button){
		int nx = (screenX-x)/50;
		int ny = (screenY-y)/50;
		
		if(ny == 0 && screenY > y){
			switch(nx){
			case INVENTORY:
				state = INVENTORY;
				GameScreen.uiManager.questUI.leave();
				break;
			case EQUIP:
				state = EQUIP;
				GameScreen.uiManager.questUI.leave();
				break;
			case SKILL:
				state = SKILL;
				GameScreen.uiManager.questUI.leave();
				break;
			case QUEST:
				state = QUEST;
				GameScreen.uiManager.questUI.leave();
				GameScreen.uiManager.questUI.enter();
			}
		}
		
		switch(state){
		case INVENTORY:
			GameScreen.player.playerInventory.clicked(screenX,screenY,button);
			break;
		case EQUIP:
			GameScreen.player.playerEquipment.clicked(screenX, screenY, button);
			break;
		case SKILL:
			GameScreen.player.playerSkills.clicked(screenX, screenY, button);
		}
	}
	
	public void mouseUp(int xClicked, int yClicked, int button){
		switch(state){
		case INVENTORY:
			GameScreen.player.playerInventory.mouseUp(xClicked, yClicked, button);
			break;
		}
	}
	
	public void mouseDragged(int mouseX, int mouseY){
		switch(state){
		case INVENTORY:
			GameScreen.player.playerInventory.mouseDragged(mouseX, mouseY);
			break;
		}
	}
}
