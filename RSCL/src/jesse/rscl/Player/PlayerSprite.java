package jesse.rscl.Player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class PlayerSprite extends Sprite {

	private static String path = "data/Player.png";

	
	
	public PlayerSprite() {
		super(new Texture(Gdx.files.internal(path)));
	}
}
