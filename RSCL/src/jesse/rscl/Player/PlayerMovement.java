package jesse.rscl.Player;

import jesse.rscl.NPC.NPC;
import jesse.rscl.Screens.GameScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.Vector2;


public class PlayerMovement {

	public direction moveDirection = null;

	public static enum direction {
		LEFT, DOWN, UP, RIGHT;
	}

	public void update() {
		if (moveDirection != null) {
			
			moveInDirection(moveDirection);
			

			moveDirection = null;
		}
	}
	
	public void moveInDirection(direction dirToMove){
		GameScreen.player.playerPosition.oldPosition.x = GameScreen.player.playerPosition.x;
		GameScreen.player.playerPosition.oldPosition.y = GameScreen.player.playerPosition.y;
		
		switch(dirToMove){
		case LEFT:
			GameScreen.player.playerPosition.x -= 1;
			GameScreen.camera.translate(-GameScreen.player.tileSize,0);
			break;
		case RIGHT:
			GameScreen.player.playerPosition.x += 1;
			GameScreen.camera.translate(GameScreen.player.tileSize,0);
			break;
		case UP:
			GameScreen.player.playerPosition.y += 1;
			GameScreen.camera.translate(0,GameScreen.player.tileSize);
			break;
		case DOWN:
			GameScreen.player.playerPosition.y -= 1;
			GameScreen.camera.translate(0,-GameScreen.player.tileSize);
			break;
		}
		
		collisionCheck();
		
		//Log position
		Gdx.app.log("x",Integer.toString((int)GameScreen.player.playerPosition.x));
		Gdx.app.log("y",Integer.toString((int)GameScreen.player.playerPosition.y));
	}
	
	public void collisionCheck(){
		//Screen bounds
		if(GameScreen.player.playerPosition.x < 0){moveInDirection(direction.RIGHT);}
		if(GameScreen.player.playerPosition.y < 0){moveInDirection(direction.UP);}
		if(GameScreen.player.playerPosition.x > GameScreen.mapManager.getWidth()/32){moveInDirection(direction.LEFT);}
		if(GameScreen.player.playerPosition.y > GameScreen.mapManager.getHeight()/32){moveInDirection(direction.DOWN);}
		
		//NPCs
		for(NPC npc : GameScreen.npcManager.NPCs){
			if(GameScreen.player.playerPosition.x == npc.npcPosition.getX() && GameScreen.player.playerPosition.y == npc.npcPosition.getY()){
				Vector2 direction = new Vector2(GameScreen.player.playerPosition.x,GameScreen.player.playerPosition.y);
				direction = direction.sub(GameScreen.player.playerPosition.oldPosition);
				direction.nor();
				
				//Horizontal
				if(direction.equals(new Vector2(1,0))){moveInDirection(PlayerMovement.direction.LEFT);}
				if(direction.equals(new Vector2(-1,0))){moveInDirection(PlayerMovement.direction.RIGHT);}
				//Vertical
				if(direction.equals(new Vector2(0,1))){moveInDirection(PlayerMovement.direction.DOWN);}
				if(direction.equals(new Vector2(0,-1))){moveInDirection(PlayerMovement.direction.UP);}
			}
		}
		
		//Collision Layer
		Cell cell = GameScreen.mapManager.collisionLayer.getCell((int)GameScreen.player.playerPosition.x, (int)GameScreen.player.playerPosition.y);
		if(cell!=null){
			Vector2 direction = new Vector2(GameScreen.player.playerPosition.x,GameScreen.player.playerPosition.y);
			direction = direction.sub(GameScreen.player.playerPosition.oldPosition);
			direction.nor();
			
			//Horizontal
			if(direction.equals(new Vector2(1,0))){moveInDirection(PlayerMovement.direction.LEFT);}
			if(direction.equals(new Vector2(-1,0))){moveInDirection(PlayerMovement.direction.RIGHT);}
			//Vertical
			if(direction.equals(new Vector2(0,1))){moveInDirection(PlayerMovement.direction.DOWN);}
			if(direction.equals(new Vector2(0,-1))){moveInDirection(PlayerMovement.direction.UP);}
		}
	}
}
