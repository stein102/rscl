package jesse.rscl.Player;

import jesse.rscl.Items.Armor;
import jesse.rscl.Items.Item;
import jesse.rscl.Items.Weapon;
import jesse.rscl.Screens.GameScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class PlayerEquipment {

	SpriteBatch batch;
	OrthographicCamera camera;
	Texture backGround;
	
	int y = 47, x = 1294;
	
	//Slot definitions
	static final int HELMET = 0;
	static final int CHEST = 1;
	static final int WEAPON = 2;
	static final int SHIELD = 3;
	static final int LEGS = 4;
	
	
	
	Item[] equipped;
	
	public PlayerEquipment() {
		equipped = new Item[5];
		
		backGround = new Texture(Gdx.files.internal("data/EquippedOutline.png"));
		
		batch = new SpriteBatch();
		camera = new OrthographicCamera();
		
		camera = new OrthographicCamera(Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight());

		camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		camera.translate(0,-4);
		camera.update();
	}
	
	public void equipWeapon(Weapon weapon){
		if(equipped[WEAPON]!=null){
			unEquipWeapon();
		}
		GameScreen.player.playerStats.addDamage(weapon.getDamage());
		equipped[WEAPON] = weapon;
	}
	
	public void equipArmor(Armor armor){
		int type = 0;
		if(armor.getSlot().equals("head")){type = HELMET;}
		if(armor.getSlot().equals("chest")){type = CHEST;}
		if(armor.getSlot().equals("legs")){type = LEGS;}
		if(armor.getSlot().equals("shield")){type = SHIELD;}
		
		if(equipped[type]!=null){
			unEquipArmor(type);
		}
		GameScreen.player.playerStats.addArmor(armor.getArmorValue());
		equipped[type] = armor;
	}
	
	public void unEquipArmor(int type) {
		if(equipped[type] != null){
			Armor armor = (Armor) GameScreen.iManager.getItem(equipped[type].getID());
			GameScreen.player.playerStats.addArmor(-armor.getArmorValue());
			GameScreen.player.playerInventory.addItem(equipped[type]);
			equipped[type] = null;
		}
		
	}

	public void render(){
		camera.update();
		
		batch.begin();
			batch.draw(backGround,x,y);
			
			if(equipped[HELMET] != null){
				batch.draw(equipped[HELMET].getSprite(),x+97,y+361);
			}
			if(equipped[CHEST] != null){
				batch.draw(equipped[CHEST].getSprite(),x+97,y+202);
			}
			if(equipped[WEAPON] != null){
				batch.draw(equipped[WEAPON].getSprite(),x+10,y+202);
			}
			if(equipped[SHIELD] != null){
				batch.draw(equipped[SHIELD].getSprite(),x+185,y+202);
			}
			if(equipped[LEGS] != null){
				batch.draw(equipped[LEGS].getSprite(),x+97,y+43);
			}
		batch.end();
	}
	
	public void unEquipWeapon(){
		if(equipped[WEAPON] != null){
			Weapon weapon = (Weapon) GameScreen.iManager.getItem(equipped[WEAPON].getID());
			GameScreen.player.playerStats.addDamage(-weapon.getDamage());
			GameScreen.player.playerInventory.addItem(equipped[WEAPON]);
			equipped[WEAPON] = null;
		}
	}
	
	public void clicked(int clickX, int clickY, int button){
		//Check for head, chest and legs in the x-axis
		if(clickX > x+97 && clickX < x+97+65){
			if(clickY>y+361 && clickY < y+361+65){
				unEquipArmor(HELMET);
			}
			if(clickY>y+202 && clickY < y+202+65){
				unEquipArmor(CHEST);
			}
			if(clickY>y+43 && clickY < y+43+65){
				unEquipArmor(LEGS);
			}
		}
		if(clickX > x+10 && clickX < x+10+65){
			if(clickY > y+202 && clickY < y+202+65){
				unEquipWeapon();
			}
		}
		if(clickX > x+185 && clickX < x+185+65){
			if(clickY > y+202 && clickY < y+202+65){
				unEquipArmor(SHIELD);
			}
		}
	}
	
	
}
