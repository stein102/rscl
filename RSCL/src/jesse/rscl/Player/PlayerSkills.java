package jesse.rscl.Player;

import jesse.rscl.Player.Skills.MiningSkill;
import jesse.rscl.Player.Skills.Skill;
import jesse.rscl.Player.Skills.WoodcuttingSkill;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class PlayerSkills {

	Texture miningTab,woodcuttingTab;
	SpriteBatch batch;
	OrthographicCamera camera;
	
	public Skill mining,woodcutting;
	
	int x = 1294, y = 501;
	int width = 130, height = 100;
	
	
	
	public PlayerSkills() {
		mining = new MiningSkill();
		woodcutting = new WoodcuttingSkill();
		
		
		batch = new SpriteBatch();
		camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		
		camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		camera.translate(0, -4);
		camera.update();
		
		
		miningTab = new Texture(Gdx.files.internal("data/miningTab.png"));
		woodcuttingTab = new Texture(Gdx.files.internal("data/woodcuttingTab.png"));
	}
	
	public void render(){
		camera.update();
		
		batch.begin();
			batch.draw(miningTab,x,y-height);
			batch.draw(woodcuttingTab,x+width,y-height);
		batch.end();
	}

	public void clicked(int clickX, int clickY, int button) {
		
		
	}
	
	
}
