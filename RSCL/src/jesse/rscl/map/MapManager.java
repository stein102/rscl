package jesse.rscl.map;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;

public class MapManager{
	
	TiledMap map;
	public TiledMapTileLayer collisionLayer,foregroundLayer;
	public TiledMapTileLayer mineLayer,treeLayer;
	OrthogonalTiledMapRenderer renderer;
	int width;
	int height;
	
	
	public MapManager() {
		map = new TmxMapLoader().load("data/maps/map.tmx");
		renderer = new OrthogonalTiledMapRenderer(map);
		
		
		//get width and Height of map
		MapProperties properties = map.getProperties();
		
		width = (properties.get("width",Integer.class)-1)*32;
		height = (properties.get("height",Integer.class)-1)*32;
		
		//Get collision layer
		collisionLayer = (TiledMapTileLayer)map.getLayers().get("Collision");
		
		//foreground
		foregroundLayer = (TiledMapTileLayer)map.getLayers().get("Foreground");
		
		
		//Layers for different nodes
		mineLayer = (TiledMapTileLayer)map.getLayers().get("Mines");
		treeLayer = (TiledMapTileLayer)map.getLayers().get("Trees");

	}
	
	public void update(OrthographicCamera cam){
		renderer.setView(cam);
		renderer.render();
	}
	
	public void getPathTo(int x1, int y1, int x2, int y2){
		
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
}
